<?php

use Illuminate\Database\Seeder;

class MatchResultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('match_results')->insert([
            'id' => '1',
            'match_id' => '1',
            'team1_score' => '3',
            'team2_score' => '0'
        ]);

        DB::table('match_results')->insert([
            'id' => '2',
            'match_id' => '2',
            'team1_score' => '2',
            'team2_score' => '2',
        ]);

        DB::table('match_results')->insert([
            'id' => '3',
            'match_id' => '3',
            'team1_score' => '1',
            'team2_score' => '4'
        ]);

        DB::table('match_results')->insert([
            'id' => '4',
            'match_id' => '4',
            'team1_score' => '3',
            'team2_score' => '3'
        ]);

        DB::table('match_results')->insert([
            'id' => '5',
            'match_id' => '5',
            'team1_score' => '2',
            'team2_score' => '0'
        ]);

        DB::table('match_results')->insert([
            'id' => '6',
            'match_id' => '6',
            'team1_score' => '4',
            'team2_score' => '3'
        ]);

        
    }
}
