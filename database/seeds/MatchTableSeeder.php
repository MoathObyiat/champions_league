<?php

use Illuminate\Database\Seeder;

class MatchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Match for week #1
        DB::table('match')->insert([
            'id' => '1',
            'week' => '1',
            'team_1' => '1',
            'team_2' => '2',
            'match_date' => '2019-08-16',
            'match_time' => '06:00'
        ]);

        DB::table('match')->insert([
            'id' => '2',
            'week' => '1',
            'team_1' => '3',
            'team_2' => '4',
            'match_date' => '2019-08-17',
            'match_time' => '04:00'
        ]);

        //Match for week #2
        DB::table('match')->insert([
            'id' => '3',
            'week' => '2',
            'team_1' => '1',
            'team_2' => '3',
            'match_date' => '2019-08-24',
            'match_time' => '03:00'
        ]);

        DB::table('match')->insert([
            'id' => '4',
            'week' => '2',
            'team_1' => '2',
            'team_2' => '4',
            'match_date' => '2019-08-26',
            'match_time' => '07:00'
        ]);

        //Match for week #3
        DB::table('match')->insert([
            'id' => '5',
            'week' => '3',
            'team_1' => '1',
            'team_2' => '4',
            'match_date' => '2019-08-24',
            'match_time' => '03:00'
        ]);

        DB::table('match')->insert([
            'id' => '6',
            'week' => '3',
            'team_1' => '2',
            'team_2' => '3',
            'match_date' => '2019-08-26',
            'match_time' => '07:00'
        ]);

    }
}
