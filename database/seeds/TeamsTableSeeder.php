<?php

use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            'id' => '1',
            'name' => 'Manchester United',
            'description' => 'Manchester United Football Club is a professional football club based in Old Trafford, Greater Manchester, England, that competes in the Premier League, the top flight of English football',
        ]);

        DB::table('teams')->insert([
            'id' => '2',
            'name' => 'Liverpool',
            'description' => 'Liverpool Football Club is a professional football club in Liverpool, England, that competes in the Premier League, the top tier of English football.',
        ]);

        DB::table('teams')->insert([
            'id' => '3',
            'name' => 'Arsenal',
            'description' => 'Arsenal Football Club is a professional football club based in Islington, London, England, that plays in the Premier League, the top flight of English football.',
        ]);

        DB::table('teams')->insert([
            'id' => '4',
            'name' => 'Chelsea',
            'description' => 'Chelsea Football Club are an English professional football club. Founded in 1905, they compete in the Premier League, the top division of English football.',
        ]);

               
    }
}
