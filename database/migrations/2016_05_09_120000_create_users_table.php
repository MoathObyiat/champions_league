<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->boolean('status');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('password', 60);
            $table->integer('role_id')->unsigned();
            $table->string('phone_1')->default("");
            $table->string('phone_2')->nullable()->default(null);
            $table->string('country')->default("");
            $table->string('city')->default("");
            $table->rememberToken();
            $table->string('created_by')->default("");
            $table->string('updated_by')->default("");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
