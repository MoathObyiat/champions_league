@extends('portal.app')

@section('content')

    {!! Form::Label('Week', 'Week:') !!}
    {!! Form::select('weeks', $weeks, null, ['class' => 'week']) !!}


    <h1>Match Result</h1>
    {{-- <form> --}}
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    {{-- {!! Form::open(['method' => 'post', 'url' => url('calculate'), 'class' => 'form-horizontal' ]) !!} --}}
        <table id='match'></table>
        <button id="calculate" onclick="sendData()" class="btn">Calculate</button>
    {{-- {!! Form::close() !!} --}}
    {{-- </form> --}}

    <h1>League Table</h1>
    <table>
        <tr>
            <th>Teams</th>
            <th>PTS</th>
            <th>P</th>
            <th>W</th>
            <th>D</th>
            <th>L</th>
            <th>GF</th>
            <th>GA</th>
            <th>GD</th>
        </tr>
        {{-- @if($teams) --}}
            @foreach($teams as $team)
                <tr>
                    <td>{{ $team->name}}</td>
                    <td>{{ $team->score['PTS']}}</td>
                    <td>{{ $team->score['P']}}</td>
                    <td>{{ $team->score['W']}}</td>
                    <td>{{ $team->score['D']}}</td>
                    <td>{{ $team->score['L']}}</td>
                    <td>{{ $team->score['GF']}}</td>
                    <td>{{ $team->score['GA']}}</td>
                    <td>{{ $team->score['GD']}}</td>
                </tr>
            @endforeach
        
        {{-- @endif --}}

    </table>
    <a href="{{ url('resetAll') }}">Reset ALL</a>
    <a href="{{ url('playAll') }}">Play ALL</a>
@endsection

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

<script>

    $(document).ready(function () {
        getDataOfWeek(1);
        $(".week").change(function(e){
            getDataOfWeek(this.value);
        });
    });
    var m1, m2;
    function getDataOfWeek(value){
        $.ajax({
            type : "GET" ,
            url : "/getData?week="+value ,
            dataType :'json',
            success : function (result) {
                var data = result.data;
                console.log(data);
                
                var html = '';
                var i;
                m1 = data[0].id;
                m2 = data[1].id;
                for (i = 0; i < data.length; i++) {
                    html+='<table><tr><td>';
                        html+= data[i]['team_one']['name'];
                        html+='</td><td>';
                        html+='<input type="text" id="team1_score_'+i+'" size="10" value="" data-team1="'+data[i].team_1+'" />' ;
                        html+='-';
                        html+='<input type="text" id="team2_score_'+i+'" size="10" value="" data-team2="'+data[i].team_2+'" /></td>' ;
                        html+= '<td>' + data[i]['team_two']['name'];
                        html+='</td></tr></table>';

                }
                $('#match').html(html);
                $('#team1_score_0').val(data[0]['match_result'] ? data[0]['match_result']['team1_score'] : '');
                $('#team2_score_0').val(data[0]['match_result'] ? data[0]['match_result']['team2_score'] : '');
                $('#team1_score_1').val(data[1]['match_result'] ? data[1]['match_result']['team1_score'] : '');
                $('#team2_score_1').val(data[1]['match_result'] ? data[1]['match_result']['team2_score'] : '');
                
            }
        })
    }

    function sendData(){
       
        var data = {
            _token: $('input[name="_token"]').val(),
            m1: m1,
            m2: m2,
            team1_score_0: $('#team1_score_0').val(),
            team2_score_0: $('#team2_score_0').val(),
            team1_score_1: $('#team1_score_1').val(),
            team2_score_1: $('#team2_score_1').val()
        }
console.log(data);

        $.ajax({
            type: "POST",
            url: '{{  url('calculate') }}',
            data: data,
            dataType: "json",
            //on success
            success: function(data){
                location.reload();
                //do something after something is recieved from php
            },
            //on error
            error: function(){
                //bad request
            }
        });
    }

</script>