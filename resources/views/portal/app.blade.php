<!DOCTYPE html>

<html class="ie9">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/css/portal/app.css" rel="stylesheet">


</head>
<body>

    @yield('content')

</body>
</html>
