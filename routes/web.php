<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'Portal\HomeController@index');

Route::get('getData', 'Portal\HomeController@ajaxData');

Route::get('resetAll', 'Portal\HomeController@resetAll');

Route::get('playAll', 'Portal\HomeController@playAll');

Route::post('calculate', [
    'uses' => 'Portal\HomeController@calculate',
    'as'   => 'calculate'
]);

