<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Models\Matches;
use App\Models\MatchesResults;
use App\Models\Teams;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function index()
    {
        $week_number  =  1;
        $match_result = Matches::query()
            ->where('week' , $week_number)
            ->with(['match_result' , 'team_one','team_two'])
            ->get();

        $teams = Teams::get();
        $weeks = Matches::distinct()->pluck('week', 'week')->toArray();
        return view('portal/home', compact(['match_result', 'teams' , 'weeks']));
    }

    public function ajaxData(Request $request)
    {
        if ($request) {
            
            $match_result = Matches::query()
            ->where('week', $request->week)
            ->with(['match_result', 'team_one', 'team_two'])
            ->get();
            
            return [
                'data' => $match_result,
                'message' => 'success',
            ];
        }
    }

    public function resetAll()
    {
        DB::table('match_results')->delete();
        return redirect('/');
    }

    public function playAll()
    {
        $matches = Matches::get(['id']);
        foreach($matches as $matche){
            MatchesResults::create([
                'match_id' => $matche->id,
                'team1_score' => rand(0,10),
                'team2_score' => rand(0,10),
            ]);
        }
        return redirect('/');
    }

    public function calculate(Request $request)
    {
        $m1 = MatchesResults::where('match_id', $request->m1)->delete();
        $m2 = MatchesResults::where('match_id', $request->m2)->delete();
        
        try {
            
            MatchesResults::create([
                'match_id' => $request->m1,
                'team1_score' => $request->team1_score_0,
                'team2_score' => $request->team2_score_0,
            ]);
    
            MatchesResults::updateOrCreate([
                'match_id' => $request->m2,
                'team1_score' => $request->team1_score_1,
                'team2_score' => $request->team2_score_1,
            ]);
    
            return [
                'message' => 'success',
            ];
        } catch (\Throwable $th) {
            //throw $th;
            return $th;
        }
    }


}