<?php

// use Auth;
// use Route;
// use App\Models\User;
use App\Repositories\UserRepository;
// use App\Models\Permission;

function isAllowed($module)
{
  // get the array of allowed controllers for the user
  $userRepository = new UserRepository();
  $allowed_controllers = $userRepository->allowedControllers(Auth::user()->role_id);

  if (is_array($allowed_controllers) && in_array($module, $allowed_controllers)) {
    return true;
  }else{
    return false;
  }
}
