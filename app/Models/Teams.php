<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    protected $table = 'teams';

    protected $appends = [
        'score'
    ];

    protected $fillable = [
        'id',
        'name',
        'description'
    ];

    public function match1()
    {
        return $this->hasMany('App\Models\Matches', 'team_1', 'id');

    }

    public function match2()
    {
        return $this->hasMany('App\Models\Matches', 'team_2', 'id');

    }

    public function getScoreAttribute()
    {
        $team_matches1 = $this->match1;
        $team_matches2 = $this->match2;

        $number_of_wins = 0;
        $number_of_tie = 0;
        $number_of_loses = 0;
        $my_goals = 0;
        $out_goals = 0;

        foreach ($team_matches1 as $result){
            if($result->match_result){
                if ($result->match_result->team1_score > $result->match_result->team2_score) {
                    $number_of_wins += 1;
                } elseif ($result->match_result->team1_score == $result->match_result->team2_score){
                    $number_of_tie += 1;
                } elseif ($result->match_result->team1_score < $result->match_result->team2_score){
                    $number_of_loses += 1;
                }
                $my_goals += $result->match_result->team1_score;
                $out_goals += $result->match_result->team2_score;

            }

        }

        foreach ($team_matches2 as $result){

            if($result->match_result){
                if ($result->match_result->team2_score > $result->match_result->team1_score) {
                    $number_of_wins += 1;
                } elseif ($result->match_result->team2_score == $result->match_result->team1_score){
                    $number_of_tie += 1;
                } elseif ($result->match_result->team2_score < $result->match_result->team1_score){
                    $number_of_loses += 1;
                }
    
                $my_goals += $result->match_result->team2_score;
                $out_goals += $result->match_result->team1_score;
            }

        }

        $goals_diff = $my_goals - $out_goals;
        $PTS =  ( $number_of_wins * 3) + $number_of_tie;

        return [
            'team_name' => $this->name,
            'PTS' => $PTS,
            'P' => count ($team_matches1) + count ($team_matches2),
            'W' => $number_of_wins,
            'D' => $number_of_tie,
            'L' => $number_of_loses,
            'GF' => $my_goals,
            'GA' => $out_goals,
            'GD' => $goals_diff
        ];
    }


}
