<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matches extends Model
{
    protected $table = 'match';

    protected $with = ['match_result'];

    protected $fillable = [
        'id',
        'week',
        'team_1',
        'team_2',
        'match_date',
        'match_time'
    ];

    public function team_one()
    {
        return $this->hasOne('App\Models\Teams', 'id' , 'team_1' );
    }

    public function team_two()
    {
        return $this->hasOne('App\Models\Teams', 'id' ,'team_2');
    }

    public function match_result()
    {
        return $this->hasOne('App\Models\MatchesResults', 'match_id');
    }






}
