<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatchesResults extends Model
{
    protected $table = 'match_results';
    protected $fillable = [
        'id',
        'match_id',
        'team1_score',
        'team2_score'
    ];

    public static function boot() {
	    parent::boot();
    }

    public function match()
    {
        return $this->belongsTo('App\Models\Matches', 'id' ,'match_id');

    }


}
